module Main where

import Network.Socket
import System.IO
import System.Process
import Control.Concurrent
import Control.Monad
import System.Environment
import System.Posix.Signals

main :: IO ()
main = do
    a <- getArgs
    let portnum = read (a!!0) :: Int
    let program = (a!!1)
    let options = tail $ tail a
    sock <- socket AF_INET Stream 0
    setSocketOption sock ReuseAddr 1
    bind sock (SockAddrInet (toEnum portnum) iNADDR_ANY)
    listen sock 2
    installHandler sigCHLD Ignore Nothing
    mainLoop sock program options

mainLoop :: Socket -> String -> [String] -> IO ()
mainLoop sock program options = do
    conn <- accept sock
    forkIO (runConn conn program options)
    mainLoop sock program options

runConn :: (Socket, SockAddr) -> String -> [String] -> IO ()
runConn (sock, _) program options = do
    hdl <- socketToHandle sock ReadWriteMode
    hSetBuffering hdl NoBuffering
    
    createProcess (proc program options) {std_out = UseHandle hdl, std_in = UseHandle hdl } 
    hClose hdl
